import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptionProfesseurComponent } from './inscription-professeur.component';

describe('InscriptionProfesseurComponent', () => {
  let component: InscriptionProfesseurComponent;
  let fixture: ComponentFixture<InscriptionProfesseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscriptionProfesseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptionProfesseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
