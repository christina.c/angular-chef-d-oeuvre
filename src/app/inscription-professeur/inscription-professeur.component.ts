import { Component, OnInit } from '@angular/core';
import { AuthService } from '../repository/auth.service';
import { User } from '../entity/user';


@Component({
  selector: 'app-inscription-professeur',
  templateUrl: './inscription-professeur.component.html',
  styleUrls: ['./inscription-professeur.component.css']
})
export class InscriptionProfesseurComponent implements OnInit {

  user: User = {
    mail: null,
    password: null,
    instruments: ''
  };
  repeat: string = null;
  message:string;

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  register() {    
    if (this.validForm()) {
      this.authService.addUser(this.user).subscribe(() => {
      //this.message = 'Vous êtes enregistré ! Mot de passe différent';
      this.repeat = null;

      this.user = {
      mail: null,
      password: null,
      instruments: '',
      }
    },
     data => this.message = data.error.message);
    }
  }

  validForm():boolean {
    return this.user.mail && this.user.password && this.user.password === this.repeat;
  }

}
