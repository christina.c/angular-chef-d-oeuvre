import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../repository/auth.service';
import { User } from "../entity/user";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;

  message:string;
  currentUser:User;

  constructor(private repoAuth:AuthService, private router:Router) { }

  ngOnInit() {
    this.repoAuth.user.subscribe(user => this.currentUser = user);
  }

  login() {
    this.repoAuth.login(this.username, this.password).subscribe(
      () => this.router.navigate(['']), 
      data => this.message = 'Mot de passe incorrect ou inscription requise pour acceder à la plateforme -> Inscription'
    )
  }

  logout() {
    this.repoAuth.logout();
    this.router.navigate(['/'])
  }
}
