import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule,  HTTP_INTERCEPTORS  } from "@angular/common/http";
import { FormsModule } from '@angular/forms'  
import { JwtService } from './interceptor/jwt.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AcueilComponent } from './acueil/acueil.component';
import { InscriptionProfesseurComponent } from './inscription-professeur/inscription-professeur.component';
import { AgendaComponent } from './agenda/agenda.component';
import { GroupeComponent } from './groupe/groupe.component';
import { InscriptionEleveComponent } from './inscription-eleve/inscription-eleve.component';
import { DocumentPartageComponent } from './document-partage/document-partage.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { CreationGroupComponent } from './creation-group/creation-group.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { SingleLinkComponent } from './single-link/single-link.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AcueilComponent,
    InscriptionProfesseurComponent,
    AgendaComponent,
    GroupeComponent,
    InscriptionEleveComponent,
    DocumentPartageComponent,
    LoginComponent,
    FooterComponent,
    CreationGroupComponent,
    SingleLinkComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FullCalendarModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: JwtService, multi: true}

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
