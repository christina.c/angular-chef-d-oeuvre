export interface Student {
    id?:number;
    name:string;
    age:string;
    level:string;
    groups:any;
}