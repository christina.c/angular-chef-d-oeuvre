export interface Group {
    id?:number;
    login:string;
    label:string;
    student?:string;
}