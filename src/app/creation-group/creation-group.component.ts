import { Component, OnInit } from '@angular/core';
import { CreationGroupService } from '../repository/creation-group.service';
import { Group } from '../entity/group';

@Component({
  selector: 'app-creation-group',
  templateUrl: './creation-group.component.html',
  styleUrls: ['./creation-group.component.css']
})
export class CreationGroupComponent implements OnInit {

  group: Group = {

    login:null,
    label:null,
    
  };
  message:string;

  constructor(private groupService: CreationGroupService) { }

  ngOnInit() {
  }
  register() {
    if (this.validForm()) {
      this.groupService.addStudent(this.group).subscribe(() => {
        this.message = 'Votre avez créez votre groupe';
        this.group =  {

          login:null,
          label:null,       
        }
      },
      data => this.message = data.error.message);
    }else {
      this.message = 'Champs Manquants';
    }
  }

  validForm():boolean {
    if(this.group.login && this.group.label) {
      return true;
    }
    return false
  }

}
