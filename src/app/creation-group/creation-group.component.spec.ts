import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationGroupComponent } from './creation-group.component';

describe('CreationGroupComponent', () => {
  let component: CreationGroupComponent;
  let fixture: ComponentFixture<CreationGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreationGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
