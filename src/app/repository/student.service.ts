import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../entity/student';


@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private url = 'http://localhost:8000/api/student';

  constructor(private http:HttpClient) { }

  findAll():Observable<Student[]> {
    return this.http.get<Student[]>(this.url);
  }

  findOne(id:number): Observable<Student[]> {
    return this.http.get<Student[]>(this.url + '/' + id);
  }

  addStudent(student:Student): Observable<Student> {
    return this.http.post<Student>(this.url, student);
  }

  update(student:Student): Observable<Student[]> {
    return this.http.put<Student[]>(this.url + '/' + student.id, student);
  }

  delete(id:number): Observable<void> {
    return this.http.delete<void>(this.url + '/' + id);
  }
}
