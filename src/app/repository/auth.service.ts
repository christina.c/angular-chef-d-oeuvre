import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from "../entity/user";
import { tap, switchMap } from "rxjs/operators";
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private url = 'http://localhost:8000/api';

  public user:BehaviorSubject<User> = new BehaviorSubject(null);
  constructor(private http:HttpClient) {}

  addUser(user:User) {
    return this.http.post<User>(this.url+'/register', user);
  }

  login(username:string, password:string) {
    return this.http.post<{token:string}>('http://localhost:8000/api/login_check', {
      username,
      password
    }).pipe(
      tap(data => localStorage.setItem('token', data.token)),
      switchMap(() => this.getUser())
    );
  }

  getToken():string {
    return localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }

  getUser(): Observable<User> {
    return this.http.get<User>(this.url).pipe(
      tap(user => this.user.next(user))
    );
  }
}
