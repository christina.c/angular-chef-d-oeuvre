import { TestBed } from '@angular/core/testing';

import { CreationGroupService } from './creation-group.service';

describe('CreationGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreationGroupService = TestBed.get(CreationGroupService);
    expect(service).toBeTruthy();
  });
});
