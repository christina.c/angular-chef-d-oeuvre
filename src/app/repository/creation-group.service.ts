import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../entity/group';

@Injectable({
  providedIn: 'root'
})
export class CreationGroupService {

  private url = 'http://localhost:8000/api/group';

  constructor(private http:HttpClient) { }

  findAll():Observable<Group[]> {
    return this.http.get<Group[]>(this.url);
  }

  findOne(id:number): Observable<Group> {
    return this.http.get<Group>(this.url + '/' + id);
  }
  findIdentifiant(identifiant:string): Observable<Group> {
    return this.http.get<Group>(this.url + '/' + identifiant);
  }

  addStudent(student:Group): Observable<Group> {
    return this.http.post<Group>(this.url, student);
  }

  update(student:Group): Observable<Group> {
    return this.http.put<Group>(this.url + '/' + student.id, student);
  }

  delete(id:number): Observable<void> {
    return this.http.delete<void>(this.url + '/' + id);
  }
}
