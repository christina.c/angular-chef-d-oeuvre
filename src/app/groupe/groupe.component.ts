import { Component, OnInit } from '@angular/core';
import { Student } from '../entity/student'
import { StudentService } from '../repository/student.service';
import { Group } from '../entity/group';
import { CreationGroupService } from '../repository/creation-group.service';

@Component({
  selector: 'app-groupe',
  templateUrl: './groupe.component.html',
  styleUrls: ['./groupe.component.css']
})
export class GroupeComponent implements OnInit {

  groups: Group[] = [];

  students: Student[] = [];

  Student:Student = {
    name:null,
    age:null,
    level:null,
    groups:[],

  }

  constructor(private repo:StudentService, private groupRepo:CreationGroupService ) { }

  ngOnInit() {
    this.repo.findAll().subscribe(data => this.students = data);
    this.groupRepo.findAll().subscribe(data => {this.groups = data;
    console.log(data);});
  }

  deleteStudent(id:number){
    this.repo.delete(id).subscribe(() => this.students = this.students.filter(item => item.id != id));
 
  }
  deleteGroup(id:number){
    this.groupRepo.delete(id).subscribe(() => this.groups = this.groups.filter(item => item.id != id));
  }
}
