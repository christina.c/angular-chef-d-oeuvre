import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentPartageComponent } from './document-partage.component';

describe('DocumentPartageComponent', () => {
  let component: DocumentPartageComponent;
  let fixture: ComponentFixture<DocumentPartageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentPartageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentPartageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
