import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InscriptionProfesseurComponent } from './inscription-professeur/inscription-professeur.component';
import { AcueilComponent } from './acueil/acueil.component';
import { AgendaComponent } from './agenda/agenda.component';
import { GroupeComponent } from './groupe/groupe.component';
import { InscriptionEleveComponent } from './inscription-eleve/inscription-eleve.component';
import { DocumentPartageComponent } from './document-partage/document-partage.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { CreationGroupComponent } from './creation-group/creation-group.component';
import { SingleLinkComponent } from './single-link/single-link.component';


const routes: Routes = [
  {path: '', component: AcueilComponent},
  {path: 'teacher', component: InscriptionProfesseurComponent},
  {path: 'diary', component: AgendaComponent},
  {path: 'group', component: GroupeComponent},
  {path: 'student', component: InscriptionEleveComponent},
  {path: 'doc', component: DocumentPartageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'footer', component: FooterComponent},
  {path: 'creationGroup', component: CreationGroupComponent},
  {path: 'groupName/:login', component: SingleLinkComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
