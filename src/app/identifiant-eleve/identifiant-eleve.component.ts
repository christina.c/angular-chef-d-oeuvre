import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CreationGroupService } from '../repository/creation-group.service';
import { Group } from '../entity/group';


@Component({
  selector: 'app-identifiant-eleve',
  templateUrl: './identifiant-eleve.component.html',
  styleUrls: ['./identifiant-eleve.component.css']
})
export class IdentifiantEleveComponent implements OnInit {

  group:Group;

  groupName:string;
  
  constructor(private route: ActivatedRoute, private repo:CreationGroupService) { }

  ngOnInit() {
    
     this.route.params.subscribe( params => {
      this.groupName = params['identifiant']; 
      this.repo.findIdentifiant(this.groupName).subscribe(group => this.group=group);
    });
  }
}