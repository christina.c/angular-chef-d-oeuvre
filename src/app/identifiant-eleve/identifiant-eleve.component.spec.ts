import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentifiantEleveComponent } from './identifiant-eleve.component';

describe('IdentifiantEleveComponent', () => {
  let component: IdentifiantEleveComponent;
  let fixture: ComponentFixture<IdentifiantEleveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentifiantEleveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentifiantEleveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
