import { Component, OnInit } from '@angular/core';
import { Student } from '../entity/student';
import { StudentService } from '../repository/student.service';
import { CreationGroupService } from '../repository/creation-group.service';
import { Group } from '../entity/group';

@Component({
  selector: 'app-inscription-eleve',
  templateUrl: './inscription-eleve.component.html',
  styleUrls: ['./inscription-eleve.component.css']
})
export class InscriptionEleveComponent implements OnInit {

  groups: Group[] = [];

  student: Student = {
    name:null,
    age:null,
    level:null,
    groups:[],
  };
  message:string;
  constructor(private studentRepo: StudentService, private groupRepo:CreationGroupService ) { }

  ngOnInit() {
    this.groupRepo.findAll().subscribe(data => this.groups = data);
  }

  register() {
    // console.log(this.student.groups)
    if (this.validForm()) {
      this.studentRepo.addStudent(this.student).subscribe(() => {
        this.message = 'Votre élève est inscrit!';
        this.student = {
          name:null,
          age:null,
          level:null,
          groups:[],
        }
      },
      
      data => this.message = data.error.message);
    }else {
      this.message = 'Certains Champs ne sont pas remplit';
    }
  }


  handleSelect(event) {
    this.student.groups = [event.target.value];
  }
  validForm():boolean {
    if(this.student.name && this.student.age && this.student.level && this.groups) {
      return true;
    }
    return false
  }


}
