import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../repository/auth.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  currentUser;

  constructor(private repoAuth:AuthService) { }

  ngOnInit() {
    this.repoAuth.user.subscribe(user => this.currentUser = user);

  }

  logout(){

    this.repoAuth.logout();
  }

}
